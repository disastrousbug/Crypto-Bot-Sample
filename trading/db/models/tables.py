from sqlalchemy import Column, String, Float, Integer

from .base import BaseModel


class StatusSymbol(BaseModel):
    __tablename__ = "status_symbol"

    symbol = Column(String(50))
    status = Column(String(50), default="BUY")
    price = Column(Float, default=0)
    buying_price = Column(Float, default=0)
    fiat_balance = Column(Float, default=20)
    stop_loss = Column(Float, default=0)
    quantity = Column(Float, default=0)
