from .base import DBSession
from .models.tables import StatusSymbol
from sqlalchemy.future import select


def get_all_symbols(session):
    symbol = session.execute(select(StatusSymbol))
    return symbol.scalars().all()
