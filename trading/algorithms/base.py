from typing import Dict, Union
import pandas as pd


class BaseAlgorithm:
    df = None
    columns_use: dict = {}
    dct_values: Dict[str, Union[int, list, pd.Series]] = None

    def __init__(self, start_price, status):
        self.start_price = start_price
        self.status = status

    def create_values(self):
        dct_values = {}
        self.df = self.df.dropna()
        for key, value in self.columns_use.items():
            if key == "price_for_if":
                dct_values[key] = self.df["close"].tail(value).values[0]
            else:
                dct_values[key] = self.df[key].tail(value).values
        self.dct_values = dct_values

    def buy_method(self):
        raise NotImplementedError

    def sell_method(self):
        raise NotImplementedError

    def get_value(self):
        self.create_values()
        if self.status == "SELL":
            if self.buy_method():
                method = "BUY"
                value = self.dct_values["price_for_if"]
            else:
                method = None
                value = self.dct_values["price_for_if"]
        else:
            if self.sell_method():
                method = "SELL"
                value = self.dct_values["price_for_if"]
            else:
                method = None
                value = self.dct_values["price_for_if"]

        return method, value


class RSISMAAlgorithm(BaseAlgorithm):
    sma_window_size: int = 24

    def sell_method(self):
        character_bool = "<"
        bool_value_sma = self.get_bool_value_sma(character_bool)

        if bool_value_sma:
            gain_count = (self.dct_values["gain"] > 0).sum()
            print(gain_count)
            change_sma = self.dct_values["change_sma"]
            price_for_if = self.dct_values["price_for_if"]
            rsi_lat = self.dct_values["rsi_14"]
            start_price = self.start_price * 1.05

            if gain_count > 7 and change_sma > 0 and price_for_if > start_price or \
                    rsi_lat > 83 and price_for_if > start_price or \
                    change_sma < 0 and price_for_if > start_price and gain_count > 7:
                bool_value = True
            else:
                bool_value = False

        else:
            bool_value = False

        return bool_value

    def get_bool_value_sma(self, character_bool):
        name = f"sma_{self.sma_window_size}"
        lst_sma = self.dct_values[name]
        bool_expression = ""
        character_bool = character_bool

        for i in range(2, len(lst_sma) - 1):
            if i == len(lst_sma) - 2:
                bool_expression += f"{lst_sma[i]} {character_bool} {lst_sma[i + 1]}"
            else:
                bool_expression += f"{lst_sma[i]} {character_bool} {lst_sma[i + 1]} and "

        bool_value = eval(bool_expression)
        return bool_value

    def buy_method(self):
        character_bool = ">"
        bool_value_sma = self.get_bool_value_sma(character_bool)

        if bool_value_sma:
            loss_count = (self.dct_values["loss"] > 0).sum()
            print(loss_count)
            change_sma = self.dct_values["change_sma"]
            rsi_lat = self.dct_values["rsi_14"]

            if loss_count > 7 and change_sma < 0 or rsi_lat < 20 or change_sma > 0 and loss_count > 7:
                bool_value = True
            else:
                bool_value = False

        else:
            bool_value = False

        return bool_value
