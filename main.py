import time
import datetime
import logging.config

from trading.base import TradeBotBase
from trading.settings import settings


logging.config.dictConfig(settings.LOGGING_CONFIG)
logger = logging.getLogger("time_process")


if __name__ == "__main__":
    while True:
        start_time = datetime.datetime.now()
        trade = TradeBotBase()
        trade.get_trade(2)
        logger.debug(f" Count processes = {4}. Time: {datetime.datetime.now() - start_time}")
        time.sleep(900 - int((datetime.datetime.now() - start_time).total_seconds()))
